package jbasto.datos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-13T17:04:40")
@StaticMetamodel(Clientes.class)
public class Clientes_ { 

    public static volatile SingularAttribute<Clientes, String> clientesNombre;
    public static volatile SingularAttribute<Clientes, String> clientesCorreo;
    public static volatile SingularAttribute<Clientes, String> clientesApellido;
    public static volatile SingularAttribute<Clientes, String> clientesCedula;
    public static volatile SingularAttribute<Clientes, String> clientesTelefono;

}