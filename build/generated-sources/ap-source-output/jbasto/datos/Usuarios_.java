package jbasto.datos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-13T17:04:40")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> correoUsuario;
    public static volatile SingularAttribute<Usuarios, String> claveUsuario;
    public static volatile SingularAttribute<Usuarios, Short> estadoUsuario;
    public static volatile SingularAttribute<Usuarios, String> nombreUsuario;
    public static volatile SingularAttribute<Usuarios, Integer> usuarioId;

}