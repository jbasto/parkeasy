package jbasto.datos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-13T17:04:40")
@StaticMetamodel(Vehiculos.class)
public class Vehiculos_ { 

    public static volatile SingularAttribute<Vehiculos, String> vehiculosClientesCedula;
    public static volatile SingularAttribute<Vehiculos, Integer> vehiculosTipo;
    public static volatile SingularAttribute<Vehiculos, String> vehiculosPlaca;

}