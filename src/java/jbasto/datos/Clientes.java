/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Clientes.findAll", query = "SELECT c FROM Clientes c")
    , @NamedQuery(name = "Clientes.findByClientesNombre", query = "SELECT c FROM Clientes c WHERE c.clientesNombre = :clientesNombre")
    , @NamedQuery(name = "Clientes.findByClientesApellido", query = "SELECT c FROM Clientes c WHERE c.clientesApellido = :clientesApellido")
    , @NamedQuery(name = "Clientes.findByClientesCorreo", query = "SELECT c FROM Clientes c WHERE c.clientesCorreo = :clientesCorreo")
    , @NamedQuery(name = "Clientes.findByClientesTelefono", query = "SELECT c FROM Clientes c WHERE c.clientesTelefono = :clientesTelefono")
    , @NamedQuery(name = "Clientes.findByClientesCedula", query = "SELECT c FROM Clientes c WHERE c.clientesCedula = :clientesCedula")})
public class Clientes implements Serializable {
  
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "clientes_nombre")
    private String clientesNombre;
    @Basic(optional = false)
    @Column(name = "clientes_apellido")
    private String clientesApellido;
    @Basic(optional = false)
    @Column(name = "clientes_correo")
    private String clientesCorreo;
    @Basic(optional = false)
    @Column(name = "clientes_telefono")
    private String clientesTelefono;
    @Id
    @Basic(optional = false)
    @Column(name = "clientes_cedula")
    private String clientesCedula;  
    
    public Clientes() {
    }

    public Clientes(String clientesCedula) {
        this.clientesCedula = clientesCedula;
    }

    public Clientes(String clientesCedula, String clientesNombre, String clientesApellido, String clientesCorreo, String clientesTelefono) {
        this.clientesCedula = clientesCedula;
        this.clientesNombre = clientesNombre;
        this.clientesApellido = clientesApellido;
        this.clientesCorreo = clientesCorreo;
        this.clientesTelefono = clientesTelefono;
    }

    public String getClientesNombre() {
        return clientesNombre;
    }

    public void setClientesNombre(String clientesNombre) {
        this.clientesNombre = clientesNombre;
    }

    public String getClientesApellido() {
        return clientesApellido;
    }

    public void setClientesApellido(String clientesApellido) {
        this.clientesApellido = clientesApellido;
    }

    public String getClientesCorreo() {
        return clientesCorreo;
    }

    public void setClientesCorreo(String clientesCorreo) {
        this.clientesCorreo = clientesCorreo;
    }

    public String getClientesTelefono() {
        return clientesTelefono;
    }

    public void setClientesTelefono(String clientesTelefono) {
        this.clientesTelefono = clientesTelefono;
    }

    public String getClientesCedula() {
        return clientesCedula;
    }

    public void setClientesCedula(String clientesCedula) {
        this.clientesCedula = clientesCedula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientesCedula != null ? clientesCedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clientes)) {
            return false;
        }
        Clientes other = (Clientes) object;
        if ((this.clientesCedula == null && other.clientesCedula != null) || (this.clientesCedula != null && !this.clientesCedula.equals(other.clientesCedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jbasto.datos.Clientes[ clientesCedula=" + clientesCedula + " ]";
    }   
}
