/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import jbasto.forms.ConfigurarBean;

/**
 *
 * @author JONATHANB
 */
public class DatabaseOperations {

    private static final String PERSISTENCE_UNIT_NAME = "ParkeasyPU";
    private static EntityManager entityMgrObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
    private static EntityTransaction transactionObj = entityMgrObj.getTransaction();
    static String cuentaYaExiste = "Exception [EclipseLink-4002]";

    @SuppressWarnings("unchecked")
    // clientes
    public static String registrarCliente(String cedula, String nombre, String apellido, String correo, String telefono) {
        Clientes c = new Clientes();
        c.setClientesCedula(cedula);
        c.setClientesNombre(nombre);
        c.setClientesApellido(apellido);
        c.setClientesCorreo(correo);
        c.setClientesTelefono(telefono);

        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        entityMgrObj.persist(c);
        try {
            transactionObj.commit();
            return "OK";
        } catch (Exception e) {
            String error = e.getLocalizedMessage();
            if (error.contains(cuentaYaExiste)) {
                return "Cliente ya Registrado";
            } else {
                return e.getLocalizedMessage();
            }
        }
    }

    public static List<Clientes> getListaClientes() {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }

        StringBuilder sql = new StringBuilder();
        sql.append("Select c from Clientes c");
        List<Clientes> clientesList = entityMgrObj.createQuery(sql.toString()).getResultList();

        if (clientesList != null && clientesList.size() > 0) {
            return clientesList;
        } else {
            return null;
        }
    }

    public static String borrarClientePorCedula(String cedulaCliente) {
        boolean result = false;
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        Clientes cObj = clienteExiste(cedulaCliente);
        if (cObj != null) {
            try {
                entityMgrObj.remove(cObj);
                result = true;
            } catch (Exception ex) {
                result = false;
            }
        }

        if (result) {
            transactionObj.commit();
            return "/faces/clientes.xhtml?faces-redirect=true";
        } else {
            transactionObj.rollback();
            return null;
        }
    }

    public static String actualizarCliente(String cedula, String nombre, String apellido, String correo, String telefono) {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        boolean result = false;

        Clientes ce = clienteExiste(cedula);

        if (ce != null) {
            Query queryObj = entityMgrObj.createQuery("UPDATE Clientes c "
                    + "SET c.clientesCedula=:cedula, "
                    + "c.clientesNombre=:nombre, "
                    + "c.clientesApellido=:apellido, "
                    + "c.clientesCorreo=:correo, "
                    + "c.clientesTelefono=:telefono "
                    + "WHERE c.clientesCedula=:cedula");

            queryObj.setParameter("cedula", cedula);
            queryObj.setParameter("nombre", nombre);
            queryObj.setParameter("apellido", apellido);
            queryObj.setParameter("correo", correo);
            queryObj.setParameter("telefono", telefono);

            int updateCount = queryObj.executeUpdate();
            if (updateCount > 0) {
                result = true;
            }
        }
        transactionObj.commit();
        if (result) {
            FacesContext.getCurrentInstance().addMessage("edit-form:cedula", new FacesMessage("Cliente " + cedula + " Actualizado en base de datos"));
        }
        return null;

    }

    public static Clientes clienteExiste(String cedulaCliente) {
        Clientes selectedCliente;
        try {
            Query q = entityMgrObj.createNamedQuery("Clientes.findByClientesCedula");
            q.setParameter("clientesCedula", cedulaCliente);
            selectedCliente = (Clientes) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

        if (selectedCliente != null) {
            return selectedCliente;
        } else {
            return null;
        }
    }

    // usuarios
    public boolean loginControl(String correoUsuario, String claveUsuario) {
        try {
            Usuarios l;
            l = entityMgrObj.createNamedQuery("Usuarios.getUsuario", Usuarios.class).setParameter("correoUsuario", correoUsuario).setParameter("claveUsuario", claveUsuario).getSingleResult();
            return l != null;
        } catch (Exception e) {
            return false;
        } finally {
            //entityMgrObj.close();
        }
    }

    public String registrarUsuario(String nombreUsuario, String correoUsuario, String claveUsuario) {
        Usuarios u = new Usuarios();
        u.setNombreUsuario(nombreUsuario);
        u.setCorreoUsuario(correoUsuario);
        u.setClaveUsuario(claveUsuario);
        u.setEstadoUsuario(Short.parseShort("1"));

        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        entityMgrObj.persist(u);
        try {
            //entityMgrObj.getTransaction().commit();
            transactionObj.commit();
            return "OK";
        } catch (Exception e) {
            String error = e.getLocalizedMessage();
            if (error.contains(cuentaYaExiste)) {
                return "Usuario ya registrado";
            } else {
                return e.getLocalizedMessage();
            }
        } //finally {                    
//            entityMgrObj.close();
//        }
    }

    // configuracion
    public String confDimParqueadero(int filas, int columnas) {
        DimensionParqueadero dp = new DimensionParqueadero();
        //dp.setIdparqueadero(0);
        dp.setFilas(filas);
        dp.setColumnas(columnas);

        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        entityMgrObj.persist(dp);
        try {
            transactionObj.commit();
            return "OK";
        } catch (Exception e) {
            String error = e.getLocalizedMessage();
            return error;
        } //finally {
        //entityMgrObj.close();
        //}                
    }

    public String getFilasColumnas(int id) {
        DimensionParqueadero dp = null;
        ConfigurarBean cb;

        try {
            dp = entityMgrObj.createNamedQuery("DimensionParqueadero.findByIdparqueadero", DimensionParqueadero.class).setParameter("idparqueadero", id).getSingleResult();
            if (dp != null) {
                cb = new ConfigurarBean();
                cb.setColumnas(String.valueOf(dp.getColumnas()));
                cb.setFilas(String.valueOf(dp.getFilas()));
                return dp.getColumnas() + "," + dp.getFilas();
            } else {
                return "0,0";
            }
        } catch (Exception e) {
            return "MAL";
        } finally {
            //entityMgrObj.close();
        }
    }

    // vehiculos
    public static List<Vehiculos> getListaVehiculos() {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }

        StringBuilder sql = new StringBuilder();
        sql.append("Select v from Vehiculos v");
        List<Vehiculos> vehiculosList = entityMgrObj.createQuery(sql.toString()).getResultList();

        if (vehiculosList != null && vehiculosList.size() > 0) {
            return vehiculosList;
        } else {
            return null;
        }
    }

    public static String registrarVehiculo(String placa, int tipo, String cedula) {
        Vehiculos v = new Vehiculos();
        v.setVehiculosPlaca(placa);
        v.setVehiculosTipo(tipo);
        v.setVehiculosClientesCedula(cedula);

        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        entityMgrObj.persist(v);
        try {
            transactionObj.commit();
            return "OK";
        } catch (Exception e) {
            String error = e.getLocalizedMessage();
            if (error.contains(cuentaYaExiste)) {
                return "Vehiculo ya Registrado";
            } else {
                return e.getLocalizedMessage();
            }
        }
    }

    public static String actualizarVehiculo(String placa, int tipo, String cedula) {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        boolean result = false;

        Vehiculos ve = vehiculoExiste(placa);

        if (ve != null) {
            Query queryObj = entityMgrObj.createQuery("UPDATE Vehiculos v "
                    + "SET v.vehiculosPlaca=:placa, "
                    + "v.vehiculosTipo=:tipo, "
                    + "v.vehiculosClientesCedula=:cedula "
                    + "WHERE v.vehiculosPlaca=:placa");

            queryObj.setParameter("placa", placa);
            queryObj.setParameter("tipo", tipo);
            queryObj.setParameter("cedula", cedula);

            int updateCount = queryObj.executeUpdate();
            if (updateCount > 0) {
                result = true;
            }
        }
        transactionObj.commit();
        if (result) {
            FacesContext.getCurrentInstance().addMessage("editveh-form:placa", new FacesMessage("Vehiculo " + cedula + " Actualizado en base de datos"));
        }
        return null;

    }

    private static Vehiculos vehiculoExiste(String placa) {
        Vehiculos selectedVehiculo;
        try {
            Query q = entityMgrObj.createNamedQuery("Vehiculos.findByVehiculosPlaca");
            q.setParameter("vehiculosPlaca", placa);
            selectedVehiculo = (Vehiculos) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

        if (selectedVehiculo != null) {
            return selectedVehiculo;
        } else {
            return null;
        }
    }

    public static String borrarVehiculoPorPlaca(String placa) {
        boolean result = false;
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
        Vehiculos vObj = vehiculoExiste(placa);
        if (vObj != null) {
            try {
                entityMgrObj.remove(vObj);
                result = true;
            } catch (Exception ex) {
                result = false;
            }
        }

        if (result) {
            transactionObj.commit();
            return "/faces/vehiculos.xhtml?faces-redirect=true";
        } else {
            transactionObj.rollback();
            return null;
        }
    }

    
    // tipo vehiculos
    public static List<TipoVehiculo> getListaTipoVehiculo() {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT t FROM TipoVehiculo t");
        List<TipoVehiculo> tipoVehiculoList = entityMgrObj.createQuery(sql.toString()).getResultList();

        if (tipoVehiculoList != null && tipoVehiculoList.size() > 0) {
            return tipoVehiculoList;
        } else {
            return null;
        }
    }

    // tarifas
    public static List<Tarifas> getListaTarifas() {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT t FROM Tarifas t");
        List<Tarifas> tarifasList = entityMgrObj.createQuery(sql.toString()).getResultList();

        if (tarifasList != null && tarifasList.size() > 0) {
            return tarifasList;
        } else {
            return null;
        }
    }
}
