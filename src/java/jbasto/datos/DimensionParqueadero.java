/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JONATHANB
 */
@Entity
@Table(name = "dimension_parqueadero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DimensionParqueadero.findAll", query = "SELECT d FROM DimensionParqueadero d")
    , @NamedQuery(name = "DimensionParqueadero.findByIdparqueadero", query = "SELECT d FROM DimensionParqueadero d WHERE d.idparqueadero = :idparqueadero")
    , @NamedQuery(name = "DimensionParqueadero.findByFilas", query = "SELECT d FROM DimensionParqueadero d WHERE d.filas = :filas")
    , @NamedQuery(name = "DimensionParqueadero.findByColumnas", query = "SELECT d FROM DimensionParqueadero d WHERE d.columnas = :columnas")})
public class DimensionParqueadero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idparqueadero")
    private Integer idparqueadero;
    @Basic(optional = false)
    @Column(name = "filas")
    private int filas;
    @Basic(optional = false)
    @Column(name = "columnas")
    private int columnas;

    public DimensionParqueadero() {
    }

    public DimensionParqueadero(Integer idparqueadero) {
        this.idparqueadero = idparqueadero;
    }

    public DimensionParqueadero(Integer idparqueadero, int filas, int columnas) {
        this.idparqueadero = idparqueadero;
        this.filas = filas;
        this.columnas = columnas;
    }

    public Integer getIdparqueadero() {
        return idparqueadero;
    }

    public void setIdparqueadero(Integer idparqueadero) {
        this.idparqueadero = idparqueadero;
    }

    public int getFilas() {
        return filas;
    }

    public void setFilas(int filas) {
        this.filas = filas;
    }

    public int getColumnas() {
        return columnas;
    }

    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idparqueadero != null ? idparqueadero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DimensionParqueadero)) {
            return false;
        }
        DimensionParqueadero other = (DimensionParqueadero) object;
        if ((this.idparqueadero == null && other.idparqueadero != null) || (this.idparqueadero != null && !this.idparqueadero.equals(other.idparqueadero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jbasto.forms.DimensionParqueadero[ idparqueadero=" + idparqueadero + " ]";
    }

}
