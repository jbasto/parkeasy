/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JONATHANB
 */
@Entity
@Table(name = "tarifas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarifas.findAll", query = "SELECT t FROM Tarifas t")
    , @NamedQuery(name = "Tarifas.findByTarifasCod", query = "SELECT t FROM Tarifas t WHERE t.tarifasCod = :tarifasCod")
    , @NamedQuery(name = "Tarifas.findByTarifasValor", query = "SELECT t FROM Tarifas t WHERE t.tarifasValor = :tarifasValor")
    , @NamedQuery(name = "Tarifas.findByTipoVehiculoCod", query = "SELECT t FROM Tarifas t WHERE t.tipoVehiculoCod = :tipoVehiculoCod")
    , @NamedQuery(name = "Tarifas.findByTarifaTipo", query = "SELECT t FROM Tarifas t WHERE t.tarifaTipo = :tarifaTipo")})
public class Tarifas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "tarifas_cod")
    private Integer tarifasCod;
    @Basic(optional = false)
    @Column(name = "tarifas_valor")
    private int tarifasValor;
    @Basic(optional = false)
    @Column(name = "tipo_vehiculo_cod")
    private int tipoVehiculoCod;
    @Basic(optional = false)
    @Column(name = "tarifa_tipo")
    private int tarifaTipo;

    public Tarifas() {
    }

    public Tarifas(Integer tarifasCod) {
        this.tarifasCod = tarifasCod;
    }

    public Tarifas(Integer tarifasCod, int tarifasValor, int tipoVehiculoCod, int tarifaTipo) {
        this.tarifasCod = tarifasCod;
        this.tarifasValor = tarifasValor;
        this.tipoVehiculoCod = tipoVehiculoCod;
        this.tarifaTipo = tarifaTipo;
    }

    public Integer getTarifasCod() {
        return tarifasCod;
    }

    public void setTarifasCod(Integer tarifasCod) {
        this.tarifasCod = tarifasCod;
    }

    public int getTarifasValor() {
        return tarifasValor;
    }

    public void setTarifasValor(int tarifasValor) {
        this.tarifasValor = tarifasValor;
    }

    public int getTipoVehiculoCod() {
        return tipoVehiculoCod;
    }

    public void setTipoVehiculoCod(int tipoVehiculoCod) {
        this.tipoVehiculoCod = tipoVehiculoCod;
    }

    public int getTarifaTipo() {
        return tarifaTipo;
    }

    public void setTarifaTipo(int tarifaTipo) {
        this.tarifaTipo = tarifaTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tarifasCod != null ? tarifasCod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarifas)) {
            return false;
        }
        Tarifas other = (Tarifas) object;
        if ((this.tarifasCod == null && other.tarifasCod != null) || (this.tarifasCod != null && !this.tarifasCod.equals(other.tarifasCod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jbasto.datos.Tarifas[ tarifasCod=" + tarifasCod + " ]";
    }
    
}
