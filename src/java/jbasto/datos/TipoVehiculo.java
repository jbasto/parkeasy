/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JONATHANB
 */
@Entity
@Table(name = "tipo_vehiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoVehiculo.findAll", query = "SELECT t FROM TipoVehiculo t")
    , @NamedQuery(name = "TipoVehiculo.findByTipoVehiculoCod", query = "SELECT t FROM TipoVehiculo t WHERE t.tipoVehiculoCod = :tipoVehiculoCod")
    , @NamedQuery(name = "TipoVehiculo.findByTipoVehiculoDescripcion", query = "SELECT t FROM TipoVehiculo t WHERE t.tipoVehiculoDescripcion = :tipoVehiculoDescripcion")})
public class TipoVehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "tipo_vehiculo_cod")
    private Integer tipoVehiculoCod;
    @Basic(optional = false)
    @Column(name = "tipo_vehiculo_descripcion")
    private String tipoVehiculoDescripcion;

    public TipoVehiculo() {
    }

    public TipoVehiculo(Integer tipoVehiculoCod) {
        this.tipoVehiculoCod = tipoVehiculoCod;
    }

    public TipoVehiculo(Integer tipoVehiculoCod, String tipoVehiculoDescripcion) {
        this.tipoVehiculoCod = tipoVehiculoCod;
        this.tipoVehiculoDescripcion = tipoVehiculoDescripcion;
    }

    public Integer getTipoVehiculoCod() {
        return tipoVehiculoCod;
    }

    public void setTipoVehiculoCod(Integer tipoVehiculoCod) {
        this.tipoVehiculoCod = tipoVehiculoCod;
    }

    public String getTipoVehiculoDescripcion() {
        return tipoVehiculoDescripcion;
    }

    public void setTipoVehiculoDescripcion(String tipoVehiculoDescripcion) {
        this.tipoVehiculoDescripcion = tipoVehiculoDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoVehiculoCod != null ? tipoVehiculoCod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoVehiculo)) {
            return false;
        }
        TipoVehiculo other = (TipoVehiculo) object;
        if ((this.tipoVehiculoCod == null && other.tipoVehiculoCod != null) || (this.tipoVehiculoCod != null && !this.tipoVehiculoCod.equals(other.tipoVehiculoCod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jbasto.datos.TipoVehiculo[ tipoVehiculoCod=" + tipoVehiculoCod + " ]";
    }
    
}
