/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.datos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JONATHANB
 */
@Entity
@Table(name = "vehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculos.findAll", query = "SELECT v FROM Vehiculos v")
    , @NamedQuery(name = "Vehiculos.findByVehiculosPlaca", query = "SELECT v FROM Vehiculos v WHERE v.vehiculosPlaca = :vehiculosPlaca")
    , @NamedQuery(name = "Vehiculos.findByVehiculosTipo", query = "SELECT v FROM Vehiculos v WHERE v.vehiculosTipo = :vehiculosTipo")
    , @NamedQuery(name = "Vehiculos.findByVehiculosClientesCedula", query = "SELECT v FROM Vehiculos v WHERE v.vehiculosClientesCedula = :vehiculosClientesCedula")})
public class Vehiculos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "vehiculos_placa")
    private String vehiculosPlaca;
    @Basic(optional = false)
    @Column(name = "vehiculos_tipo")
    private int vehiculosTipo;
    @Basic(optional = false)
    @Column(name = "vehiculos_clientes_cedula")
    private String vehiculosClientesCedula;

    public Vehiculos() {
    }

    public Vehiculos(String vehiculosPlaca) {
        this.vehiculosPlaca = vehiculosPlaca;
    }

    public Vehiculos(String vehiculosPlaca, int vehiculosTipo, String vehiculosClientesCedula) {
        this.vehiculosPlaca = vehiculosPlaca;
        this.vehiculosTipo = vehiculosTipo;
        this.vehiculosClientesCedula = vehiculosClientesCedula;
    }

    public String getVehiculosPlaca() {
        return vehiculosPlaca;
    }

    public void setVehiculosPlaca(String vehiculosPlaca) {
        this.vehiculosPlaca = vehiculosPlaca;
    }

    public int getVehiculosTipo() {
        return vehiculosTipo;
    }

    public void setVehiculosTipo(int vehiculosTipo) {
        this.vehiculosTipo = vehiculosTipo;
    }

    public String getVehiculosClientesCedula() {
        return vehiculosClientesCedula;
    }

    public void setVehiculosClientesCedula(String vehiculosClientesCedula) {
        this.vehiculosClientesCedula = vehiculosClientesCedula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehiculosPlaca != null ? vehiculosPlaca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculos)) {
            return false;
        }
        Vehiculos other = (Vehiculos) object;
        if ((this.vehiculosPlaca == null && other.vehiculosPlaca != null) || (this.vehiculosPlaca != null && !this.vehiculosPlaca.equals(other.vehiculosPlaca))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jbasto.datos.Vehiculos[ vehiculosPlaca=" + vehiculosPlaca + " ]";
    }
    
}
