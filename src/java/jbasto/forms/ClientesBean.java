/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import jbasto.datos.Clientes;
import jbasto.datos.DatabaseOperations;
import static jbasto.forms.LoginBean.mostrsrMsg;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Jonathan
 */
@ManagedBean
@RequestScoped
public class ClientesBean {

    private String clientesCedula, clientesNombre, clientesApellido, clientesCorreo, clientesTelefono;
    private final DatabaseOperations dao = new DatabaseOperations();
    private String editclientesCedula;

    /**
     * Creates a new instance of ClientesBeam
     */
    public ClientesBean() {
    }

    public String getClientesCedula() {
        return clientesCedula;
    }

    public void setClientesCedula(String clientesCedula) {
        this.clientesCedula = clientesCedula;
    }

    public String getClientesNombre() {
        return clientesNombre;
    }

    public void setClientesNombre(String clientesNombre) {
        this.clientesNombre = clientesNombre;
    }

    public String getClientesApellido() {
        return clientesApellido;
    }

    public void setClientesApellido(String clientesApellido) {
        this.clientesApellido = clientesApellido;
    }

    public String getClientesCorreo() {
        return clientesCorreo;
    }

    public void setClientesCorreo(String clientesCorreo) {
        this.clientesCorreo = clientesCorreo;
    }

    public String getClientesTelefono() {
        return clientesTelefono;
    }

    public void setClientesTelefono(String clientesTelefono) {
        this.clientesTelefono = clientesTelefono;
    }

    public String getEditclientesCedula() {
        return editclientesCedula;
    }

    public void setEditclientesCedula(String editclientesCedula) {
        this.editclientesCedula = editclientesCedula;
    }

    public List listaClientesDb() {
        return DatabaseOperations.getListaClientes();
    }

    public Clientes getClienteCedula(String cedula){
    return DatabaseOperations.clienteExiste(cedula);
    }
    
    public String registrarCliente() {
        if (clientesCedula == null || clientesCedula.length() == 0
                && clientesNombre == null || clientesNombre.length() == 0
                && clientesApellido == null || clientesApellido.length() == 0
                && clientesCorreo == null || clientesCorreo.length() == 0
                && clientesTelefono == null || clientesTelefono.length() == 0) {
            mostrsrMsg("Todos los campos son obligatorios", 1);
            return null;
        } else {
            String queryResult = DatabaseOperations.registrarCliente(clientesCedula, clientesNombre, clientesApellido, clientesCorreo, clientesTelefono);
            if (queryResult.equalsIgnoreCase("OK")) {
                mostrsrMsg("Registro exitoso!", 0);
                return null;
            } else if (queryResult.equals("Cliente ya Registrado")) {
                mostrsrMsg(queryResult, 2);
                return null;
            } else {
                mostrsrMsg(queryResult, 2);
                return null;
            }
        }
    }

    public String editarClientePorCedula() {
        this.editclientesCedula = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedclientesCedula");
        return "/faces/editarCliente.xhtml?faces-redirect=true";//"schoolEdit.xhtml";
    }

    public String borrarClientePorCedula(String cedulaCliente) {
        return DatabaseOperations.borrarClientePorCedula(cedulaCliente);
    }

    public String actualizarCliente(ClientesBean c) {
        return DatabaseOperations.actualizarCliente(c.getClientesCedula(), c.getClientesNombre(), c.getClientesApellido(), c.getClientesCorreo(), c.getClientesTelefono());
    }

    public String volverLista() {
        return "/faces/clientes.xhtml?i=1faces-redirect=true";
    }
    
    public String editarClienteDialog() {
        this.editclientesCedula = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clientesCedula");
      
        /*Map<String, Object> options = new HashMap<>();
        options.put("modal", true);
        options.put("draggable", true);
        options.put("resizable", true);
        options.put("includeViewParams", true);
        RequestContext.getCurrentInstance().openDialog("/faces/editarCliente", options, null);*/
           
        RequestContext.getCurrentInstance().openDialog("editarCliente");
        return "";
    }
}
