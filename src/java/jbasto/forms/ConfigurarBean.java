/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import jbasto.datos.DatabaseOperations;
import jbasto.datos.DimensionParqueadero;
import static jbasto.forms.LoginBean.mostrsrMsg;

/**
 *
 * @author JONATHANB
 */
@ManagedBean(name = "configurar")
@RequestScoped
public class ConfigurarBean implements Serializable {

    private String filas, columnas;
    private final DatabaseOperations dao = new DatabaseOperations();
    DimensionParqueadero dp = new DimensionParqueadero();

    /**
     * Creates a new instance of ConfigurarBeam
     */
    public ConfigurarBean() {
    }

    public String getFilas() {
        return filas;
    }

    public void setFilas(String filas) {
        this.filas = filas;
    }

    public String getColumnas() {
        return columnas;
    }

    public void setColumnas(String columnas) {
        this.columnas = columnas;
    }

    public String configurarDimension() {
        String msg;
        int tipo;
        int iFilas = 0, iCols = 0;

        if (filas == null || filas.length() == 0
                && columnas == null || columnas.length() == 0) {
            mostrsrMsg("Todos los campos son obligatorios", 1);
            return null;
        } else {
            iFilas = Integer.parseInt(filas);
            iCols = Integer.parseInt(columnas);

            String queryResult = dao.confDimParqueadero(iFilas, iCols);
            if (queryResult.equalsIgnoreCase("OK")) {
                mostrsrMsg("Registro exitoso!", 0);
                return null;
            } else {
                mostrsrMsg(queryResult, 2);
                return null;
            }
        }
    }
}
