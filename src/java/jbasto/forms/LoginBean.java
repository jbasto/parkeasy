/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import jbasto.datos.DatabaseOperations;

/**
 *
 * @author JONATHANB
 */
@ManagedBean(name = "login")
@RequestScoped
public class LoginBean implements Serializable {

    private String correo, contrasena, nombreDeUsuario;
    private final DatabaseOperations dao = new DatabaseOperations();

    /**
     * Creates a new instance of Login
     */
    public LoginBean() {
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombreDeUsuario() {
        return nombreDeUsuario;
    }

    public void setNombreDeUsuario(String nombreDeUsuario) {
        this.nombreDeUsuario = nombreDeUsuario;
    }

    public String iniciarSesion() {      
        if (correo == null || correo.length() == 0 && contrasena == null || contrasena.length() == 0) {                 
            mostrsrMsg("El nombre de usuario y contraseña no pueden estar vacíos.", 2);
            return null;
        } else if (dao.loginControl(correo, contrasena)) {
            return "/faces/index.xhtml?faces-redirect=true";
        } else {
            mostrsrMsg("Credenciales erróneas!", 2);
            return null;
        }
    }

    public String registrar() {
        return "/faces/registro.xhtml?faces-redirect=true";
    }

    public static void mostrsrMsg(String msg, int tipo) {
        FacesMessage.Severity sv = null;
        switch (tipo) {
            case 0:
                sv = FacesMessage.SEVERITY_INFO;
                break;
            case 1:
                sv = FacesMessage.SEVERITY_ERROR;
                break;
            case 2:
                sv = FacesMessage.SEVERITY_WARN;
                break;
        }
        FacesMessage f = new FacesMessage(sv, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, f);
    }
}
