/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import jbasto.datos.DatabaseOperations;
import static jbasto.forms.LoginBean.mostrsrMsg;

/**
 *
 * @author JONATHANB
 */
@ManagedBean(name = "registro")
@RequestScoped
public class RegistroBean {

    String nombreUsuario, correoUsuario, claveUsuario, conClaveUsuario;
    DatabaseOperations dao = new DatabaseOperations();

    /**
     * Creates a new instance of RegistroBeam
     */
    public RegistroBean() {
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public String getConClaveUsuario() {
        return conClaveUsuario;
    }

    public void setConClaveUsuario(String conClaveUsuario) {
        this.conClaveUsuario = conClaveUsuario;
    }

    public String registrar() {
        String msg;
        int tipo;

        if (nombreUsuario == null || nombreUsuario.length() == 0
                && correoUsuario == null || correoUsuario.length() == 0
                && claveUsuario == null || claveUsuario.length() == 0
                && conClaveUsuario == null || conClaveUsuario.length() == 0) {
            mostrsrMsg("Todos los campos son obligatorios", 1);
            return null;
        } else if (claveUsuario.equals(conClaveUsuario)) {
            String queryResult = dao.registrarUsuario(nombreUsuario, correoUsuario, claveUsuario);
            if (queryResult.equalsIgnoreCase("OK")) {
                mostrsrMsg("Registro exitoso!", 0);
                return null;
            } else if (queryResult.equals("Usuario ya registrado")) {
                mostrsrMsg(queryResult, 2);
                return null;
            } else {
                mostrsrMsg(queryResult, 2);
                return null;
            }
        } else {
            mostrsrMsg("Las contraseñas no coinciden", 2);
            return null;
        }
    }
}
