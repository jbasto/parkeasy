/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import jbasto.datos.DatabaseOperations;

/**
 *
 * @author JONATHANB
 */
@ManagedBean
@RequestScoped
public class TarifasBean {

    private Integer tarifasCod;
    private int tarifasValor;
    private int tipoVehiculoCod;
    private int tarifaTipo;

    /**
     * Creates a new instance of TarifasBean
     */
    public TarifasBean() {
    }

    public Integer getTarifasCod() {
        return tarifasCod;
    }

    public void setTarifasCod(Integer tarifasCod) {
        this.tarifasCod = tarifasCod;
    }

    public int getTarifasValor() {
        return tarifasValor;
    }

    public void setTarifasValor(int tarifasValor) {
        this.tarifasValor = tarifasValor;
    }

    public int getTipoVehiculoCod() {
        return tipoVehiculoCod;
    }

    public void setTipoVehiculoCod(int tipoVehiculoCod) {
        this.tipoVehiculoCod = tipoVehiculoCod;
    }

    public int getTarifaTipo() {
        return tarifaTipo;
    }

    public void setTarifaTipo(int tarifaTipo) {
        this.tarifaTipo = tarifaTipo;
    }

    public List listaTarifasDb() {
        return DatabaseOperations.getListaTarifas();
    }
}
