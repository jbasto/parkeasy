/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import jbasto.datos.DatabaseOperations;

/**
 *
 * @author JONATHANB
 */
@ManagedBean
@RequestScoped
public class TipoVehiculoBean {

    private Integer tipoVehiculoCod;
    private String tipoVehiculoDescripcion;

    /**
     * Creates a new instance of TipoVehiculoBean
     */
    public TipoVehiculoBean() {
    }

    public Integer getTipoVehiculoCod() {
        return tipoVehiculoCod;
    }

    public void setTipoVehiculoCod(Integer tipoVehiculoCod) {
        this.tipoVehiculoCod = tipoVehiculoCod;
    }

    public String getTipoVehiculoDescripcion() {
        return tipoVehiculoDescripcion;
    }

    public void setTipoVehiculoDescripcion(String tipoVehiculoDescripcion) {
        this.tipoVehiculoDescripcion = tipoVehiculoDescripcion;
    }

    public List listaTipoVehiculoDb() {
        return DatabaseOperations.getListaTipoVehiculo();
    }
}
