/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbasto.forms;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import jbasto.datos.DatabaseOperations;
import static jbasto.forms.LoginBean.mostrsrMsg;
import org.primefaces.context.RequestContext;

/**
 *
 * @author JONATHANB
 */
@ManagedBean
@RequestScoped
public class VehiculosBean {

    private String vehiculosPlaca;
    private int vehiculosTipo;
    private String vehiculosClientesCedula;
    private final DatabaseOperations dao = new DatabaseOperations();
    private String editvehiculoPlaca;

    /**
     * Creates a new instance of VehiculosBean
     */
    public VehiculosBean() {
    }

    public String getVehiculosPlaca() {
        return vehiculosPlaca;
    }

    public void setVehiculosPlaca(String vehiculosPlaca) {
        this.vehiculosPlaca = vehiculosPlaca;
    }

    public int getVehiculosTipo() {
        return vehiculosTipo;
    }

    public void setVehiculosTipo(int vehiculosTipo) {
        this.vehiculosTipo = vehiculosTipo;
    }

    public String getVehiculosClientesCedula() {
        return vehiculosClientesCedula;
    }

    public void setVehiculosClientesCedula(String vehiculosClientesCedula) {
        this.vehiculosClientesCedula = vehiculosClientesCedula;
    }

    public String getEditvehiculoPlaca() {
        return editvehiculoPlaca;
    }

    public void setEditvehiculoPlaca(String editvehiculoPlaca) {
        this.editvehiculoPlaca = editvehiculoPlaca;
    }

    public List listaVehiculosDb() {
        return DatabaseOperations.getListaVehiculos();
    }

    public String registrarVehiculo() {
        if (vehiculosPlaca == null || vehiculosPlaca.length() == 0
                && vehiculosTipo >= 0
                && vehiculosClientesCedula == null || vehiculosClientesCedula.length() == 0) {
            mostrsrMsg("Todos los campos son obligatorios", 1);
            return null;
        } else {
            String queryResult = DatabaseOperations.registrarVehiculo(vehiculosPlaca, vehiculosTipo, vehiculosClientesCedula);
            if (queryResult.equalsIgnoreCase("OK")) {
                mostrsrMsg("Registro exitoso!", 0);
                return null;
            } else if (queryResult.equals("Vehiculo ya Registrado")) {
                mostrsrMsg(queryResult, 2);
                return null;
            } else {
                mostrsrMsg(queryResult, 2);
                return null;
            }
        }
    }

    public String editarVehiculoPlaca() {
        editvehiculoPlaca = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedPlaca");
        return "/faces/editarVehiculo.xhtml";//?faces-redirect=true";//"schoolEdit.xhtml";
    }

    public String actualizarVehiculo(VehiculosBean v) {
        return DatabaseOperations.actualizarVehiculo(v.getVehiculosPlaca(), v.getVehiculosTipo(), v.getVehiculosClientesCedula());
    }

    public String volverLista() {
        return "/faces/vehiculos.xhtml?i=2faces-redirect=true";
    }

    public String borrarVehiculoPorPlaca(String placa) {
        return DatabaseOperations.borrarVehiculoPorPlaca(placa);
    }

    public String editarVehiculoDialog() {
        this.vehiculosPlaca = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedPlaca");

        /*Map<String, Object> options = new HashMap<>();
        options.put("modal", true);
        options.put("draggable", true);
        options.put("resizable", true);
        options.put("includeViewParams", true);
        RequestContext.getCurrentInstance().openDialog("/faces/editarCliente", options, null);*/
        RequestContext.getCurrentInstance().openDialog("editarVehiculo");
        return "";
    }
}
